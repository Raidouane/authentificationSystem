let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    username: String,
    email: String,
    hashed_password: String,
    salt: String,
    temp_str: String
});

module.exports = mongoose.model('users', userSchema);