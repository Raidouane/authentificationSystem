
let jwt = require('jwt-simple');

exports.gen_token = (data) => {
    let dateObj = new Date();
    let expiration_delay = dateObj.setDate(dateObj.getDate() + 60);

    let token = jwt.encode({
        expiration: expiration_delay,
        id: data._id
    }, require('./config').secret());

    return token;
};