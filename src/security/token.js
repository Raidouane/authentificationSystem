function response_no_rights(res) => {
    res.status(401);
    res.json({
        'error': 401,
        'message': null
    });
    return false;
}

let security = {
    check: (req, res, callback, passphrase, security_level) => {

        let jwt = require('jwt-simple');
        let request_header = req.headers;
        let id = request_header['x-key'];
        let token = request_header['x-access-token'];

        if (!id || !token) {
            return response_no_rights(res); // header not set
        }

        try {
            var token_decoded = jwt.decode(token, passphrase);
        } catch (e) {
            return response_no_rights(res);
        }

        if (!token_decoded) {
            return response_no_rights(res);

        } else if (token_decoded.id != id) {
            return response_no_rights(res);

        } else if (token_decoded.expiration <= Date.now()) {
            return response_no_rights(res);
        }

        let user = require('../../schemas/user');

        user.findOne({ _id: id })
            .exec((err, user) => {

                if (err || !user) {

                    return response_no_rights(res);

                } else if (user._id != token_decoded.id ||
                    user.security_level < security_level) {

                    return response_no_rights(res);
                }
                callback(req, res);
            });
    }
};

module.exports = security;