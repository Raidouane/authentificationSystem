/**
 *
 * @api {POST} sign/in SignIn
 * @apiVersion 0.0.1
 * @apiName SignIn
 * @apiGroup Sign
 *
 *
 * @apiParam Body
 * @apiParam {String} id The email of user or his username
 * @apiParam {String} password password of user
 * @apiParamExample {url} Sign In
 *      POST /api/sign/in
 *
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 400,
 *          'response': "platform missed."
 *      }
 *
 * @apiSuccess 200 You can catch the token of user, the id, and his status(his security level)
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'token': new_token,
 *            'id': data._id
 *     }
 */

let users = require('../../schemas/user');
let crypto = require('crypto');

let do_connection = (err, data, req, res) => {

    let passwd = req.body.password;

    if (err) {

        res.status(500);
        res.json({
            'error': 500
        });
    } else if (!data) {

        res.status(400);
        res.json({
            'error': 1003
        });

    } else {
        let temp = data.salt;
        let hash_db = data.hashed_password;
        let newpass = temp + passwd;
        let password = crypto.createHash('sha512').update(newpass).digest("hex");

        if (hash_db == password) {

            let new_token = require('../security/GenerateToken').gen_token(data);

            if (!data.activate)
                data.activate = true;
            data.save();

            res.status(200).json({
                'error': 0,
                'token': new_token,
                'id': data._id
            });

        } else {

            res.status(400).json({
                'error': 400
            });

        }
    }
};

module.exports = (req, res) => {

    let passwd = req.body.password;
    let identifiant =  req.body.id;

    if (!passwd) {
        res.status(400);
        res.json({
            'error': 400
        });
        return;
    }

    if (identifiant && (!(identifiant.indexOf("@") == identifiant.length) && identifiant.match(/([@])/))) {

        users.findOne({ email: identifiant })
            .exec((err, data) => {

                return do_connection(err, data, req, res);
            });

    } else{

        users.findOne({ username: identifiant })
            .exec((err, data) => {
                return do_connection(err, data, req, res);
            });
    }
};