let express = require('express');
let router = express.Router();

router.post('/up', require('./up'));
router.post('/in', require('./in'));

module.exports = router;
