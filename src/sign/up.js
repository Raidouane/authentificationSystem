/**
 *
 * @api {POST} sign/up SignUp
 *
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} username The username of user
 * @apiParam {String} password password of user

 * @apiParamExample {url} Sign Up
 *      POST /api/sign/up
 *
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 1007,
 *          'response': "Password Weak"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0
 *     }
 */

let crypto = require('crypto');
let rand = require('csprng');
let user = require('../../schemas/user');

module.exports = (req, res) => {

    let email = req.body.email;

    if (!req.body.username || !req.body.password || !email) {
        res.status(400);
        res.json({
            'error': 400,
            'response': "paramaters missed"
        });
        return;
    }

    if (req.body.username.length > 15 || req.body.username.match(/[\s]/) || req.body.username.match(/[\t]/) ){
        res.status(400);
        res.json({
            'error': 1010,
            'response': "Username Syntax is not correct"
        });
        return;
    }

    console.log(email.indexOf("@"));
    if ((!(email.indexOf("@") == email.length) && email.match(/([@])/))) {

        if (req.body.password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) &&
            req.body.password.length > 4 && req.body.password.match(/[0-9]/)) {

            let temp = rand(160, 20);
            let newpass = temp + req.body.password;
            let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex");

            user.findOne({ email: req.body.email })
                .exec( (err, users) => {

                    if (!users) {
                        user.findOne({
                            username: req.body.username.toLowerCase()
                        }).exec((err, users) => {
                            if (!users) {

                                let newuser = new user({
                                    username: req.body.username,
                                    email: req.body.email,
                                    hashed_password: hashed_password,
                                    salt: temp
                                });

                                newuser.save((err) => {
                                    if (err) {
                                        res.status(500).json({
                                            'error': 500
                                        });
                                    }
                                    res.status(201).json({
                                        'error': 0
                                    });
                                });

                            } else {
                                res.status(400);
                                res.json({
                                    'error': 1009,
                                    'response': "username already registered"
                                });
                        }
                        });
                    } else {

                        res.status(400);
                        res.json({
                            'error': 1005,
                            'response': "Email already Registered"
                        });
                    }
                });
        } else {
            res.status(400);
            res.json({
                'error': 1007,
                'response': "Password Weak"
            });
        }
    } else {
        res.status(400);
        res.json({
            'error': 400,
            'response': "Email Not Valid"
        });
    }
};
